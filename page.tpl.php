<?php // $Id$ ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language; ?>">
<?php $isadmin = (arg(0) == 'admin' || (arg(0) == 'node' && arg(1) == 'add') || (arg(0) == 'node' && arg(2) == 'edit') || (arg(0) == 'user' && arg(2) == 'edit')); ?>
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body <?php print theme("onload_attribute"); ?>>
<!-- header starts-->
<div id="header-wrap"><div id="header" class="container_16">						
	
	<!-- EMB - LOGO -->
	<h1 id="logo-text">
	<a href="<?php print url(); ?>">
	<?php if ($logo) { ?>
	<img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" />
	<?php } ?>
	<?php print $site_name; ?></a></h1>		
	<?php if ($site_slogan) { ?>
	<p id="intro"><?php print $site_slogan; ?></p>				
	<?php } ?>
	
	<!-- navigation -->
	<div  id="nav">
	       <?php print theme('links', $primary_links); ?>
	</div>		
	
	<div id="header-image"></div> 		
	
        <?php if ($search_box) { ?>
        <p><?php print($search_box); ?></p>
        <?php } ?>

<!-- header ends here -->
</div></div>

<!-- content starts -->
<div id="content-outer"><div id="content-wrapper" class="container_16">


	<!-- main -->
	<div id="main" class="<?= $isadmin ? "grid_12" : "grid_8" ?>">

	      <?php print $messages; ?>
	      <?php print $help; ?>
	      <?php print $tabs; ?>
	      <?php print $content; ?>

             <?php if ($content_bottom_left || $content_bottom_right): ?>
	     <div id="content-bottom" class="<?= $isadmin ? "grid_12" : "grid_8" ?>">
	      
		  <div id="content-bottom-left" class="grid_4 alpha"><?php echo $content_bottom_left; ?></div>

		  <div id="content-bottom-right" class="grid_4 omega"><?php echo $content_bottom_right; ?></div>

	     </div>
      <?php endif; ?>

	<!-- main ends -->
	</div>
	
	<!-- left-columns starts -->
	<div id="left-columns" class="<?= $isadmin ? "grid_4" : "grid_8" ?>">
	        <?php if(!$isadmin) { ?>
		<div class="grid_4 alpha">
			<?php print $sidebar_left ; ?>
		</div>
		<?php } ?>
	
		<div class="grid_4 omega">
			<?php print $sidebar_right ; ?>
		</div>	
	
	<!-- end left-columns -->
	</div>		
	
<!-- contents end here -->	
</div></div>

<!-- footer starts here -->	
<div id="footer-wrapper" class="container_16">

	<div id="footer-content">
	
		<div class="grid_8">

			<?php if ($footer_left): ?>
				<div id="footer-left"><?php echo $footer_left; ?></div>
			<?php endif; ?>
			
			<? /*
			<h3>Resource Links</h3>			
			<p>
			Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec libero. Suspendisse bibendum. 
			Cras id urna. Morbi tincidunt, orci ac convallis aliquam, lectus turpis varius lorem, eu 
			posuere nunc justo tempus leo. Donec mattis, purus nec placerat bibendum, dui pede condimentum 
			odio, ac blandit ante orci ut diam. <a href="index.html">Read more...</a>
			</p>
		
			<ul class="footer-list">				
				<li><a href="http://themeforest.net?ref=ealigam">ThemeForest - <span>Your Choice for Site Templates &amp; Wordpress Themes</span></a></li>
				<li><a href="http://www.cssmania.com/">CSSMania - <span>CSS Design Showcase</span></a></li>
				<li><a href="http://www.alistapart.com/">AListApart - <span>For People Who Make Websites</span></a></li>
				<li><a href="http://www.psdtuts.com/">PSDTuts.com - <span>Photoshop Tutorials And Links</span></a></li>
				<li><a href="http://www.pdphoto.org/">PDPhoto.org - <span>Public Domain Photos</span></a></li>						
				<li><a href="http://www.freephotos.se/">FreePhotos.se - <span>Free &amp; Public Domain Photos</span></a></li>	
				<li><a href="http://www.fotolia.com/partner/114283">Fotolia - <span>Free stock images or from $1</span></a></li>						
				<li><a href="http://www.4templates.com/?aff=ealigam">4templates - <span>Low Cost Hi-Quality Templates</span></a></li>	
			</ul>			
			*/ ?>		
		</div>
		
		<div class="grid_8">

			<?php if ($footer_right): ?>
				<div id="footer-right"><?php echo $footer_right; ?></div>
			<?php endif; ?>
			<? /*
			<h3>Image Gallery </h3>					
			<p class="thumbs">
				<a href="index.html"><img src="images/thumb.jpg" width="40" height="40" alt="thumbnail" /></a>
				<a href="index.html"><img src="images/thumb.jpg" width="40" height="40" alt="thumbnail" /></a>
				<a href="index.html"><img src="images/thumb.jpg" width="40" height="40" alt="thumbnail" /></a>
				<a href="index.html"><img src="images/thumb.jpg" width="40" height="40" alt="thumbnail" /></a>
				<a href="index.html"><img src="images/thumb.jpg" width="40" height="40" alt="thumbnail" /></a>
				<a href="index.html"><img src="images/thumb.jpg" width="40" height="40" alt="thumbnail" /></a>	
				<a href="index.html"><img src="images/thumb.jpg" width="40" height="40" alt="thumbnail" /></a>
				<a href="index.html"><img src="images/thumb.jpg" width="40" height="40" alt="thumbnail" /></a>													
				<a href="index.html"><img src="images/thumb.jpg" width="40" height="40" alt="thumbnail" /></a>
				<a href="index.html"><img src="images/thumb.jpg" width="40" height="40" alt="thumbnail" /></a>
				<a href="index.html"><img src="images/thumb.jpg" width="40" height="40" alt="thumbnail" /></a>													
				<a href="index.html"><img src="images/thumb.jpg" width="40" height="40" alt="thumbnail" /></a>
			</p>	
		
			<h3>About</h3>			
			<p>
			<a href="http://getfirefox.com/"><img src="images/gravatar.jpg" width="40" height="40" alt="firefox" class="float-left" /></a>
			Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec libero. Suspendisse bibendum. 
			Cras id urna. Morbi tincidunt, orci ac convallis aliquam, lectus turpis varius lorem, eu 
			posuere nunc justo tempus leo. Donec mattis, purus nec placerat bibendum, dui pede condimentum 
			odio, ac blandit ante orci ut diam.</p>
			<p>
			Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec libero. Suspendisse bibendum. 
			Cras id urna. <a href="index.html">Learn more...</a></p>
			*/ ?>
		</div>	
	
	</div>

	<div id="footer-bottom">

		<p class="bottom-left">			
			<?php print $footer_message; ?>
			<!-- TODO -->
		</p>	
		
		<p class="bottom-right">
		design by <a href="http://www.styleshout.com/">styleshout</a>	&nbsp; | &nbsp;
		drupalized by <a href="http://www.emailmarketingblog.it">email marketing blog</a>
		</p>
			<!-- TODO -->
			<? /*
		<p class="bottom-right" >
			<a href="index.html">Home</a> |
			<a href="index.html">Sitemap</a> |
			<a href="index.html">RSS Feed</a> |						
			<a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> | 
	   	<a href="http://validator.w3.org/check/referer">XHTML</a>
		</p>
		*/ ?>
	</div>	
		
</div>
<!-- footer ends here -->

<?php echo $closure; ?>
</body>
</html>