<?php // $Id$ ?>
  <div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">

    <!-- <div class="contenttitle"> -->
    <?php if ($title) : ?>
      <h2 class="title node-title withcontentdate"><?php if (!$node->ctreeischild) { ?><a href="<?php print $node_url?>"><?php print $title?></a><? } else { ?><?php print $title; ?><? } ?></h2>
    <?php endif; ?>
    <?php if ($page == 0): ?>
    <?php endif; ?>

    <?php if ($picture): ?>
    <div class="picture"><?php echo $picture; ?></div>
    <?php endif; ?>

    <?php if ($type == 'blog' || $type == 'feed_post') { ?>
    <div class="contentdate">
      <h3><?php print $month; ?></h3>
      <h4><?php print $day; ?></h4>
    </div>
    <?php } ?>

    <?php if ($submitted): ?>
      <div class="submitted">
        <?php print $submitted; ?>
      </div>
    <?php endif; ?>

    <p class="post-info">
    <?php if (isset($terms)) { ?><?php print t('Submitted in ').$terms; ?><?php if (isset($tags)) { ?><br/><?php } ?><?php } ?>
    <?php if (isset($tags)) { ?><span class="tagslabel taxonomy"></span><?php print($tags); ?><?php } ?>
    </p>
    <!-- </div> -->
    <div class="content"><?php print $content?>
    <div class="clear"></div>
    </div>

    <?php if ($links) { ?>
    <div class="postmeta links">
    	<?php print $links?>
    </div>
    <?php }; ?>

  </div>

  <div style="clear: both;"></div>
  <div class="postspace">

  </div>
