<?php // $Id$ ?><?php
/**
 * @file
 * Template.php for keepitsimple theme.
 */

/*******************************************************************************
 * 
 ******************************************************************************/

function phptemplate_search_theme_form($form) {
  /**
   * This snippet catches the default searchbox and looks for
   * search-theme-form.tpl.php file in the same folder
   * which has the new layout.
   */
  return _phptemplate_callback('search_theme_form', array('form' => $form), array('search-theme-form'));
}

/**
 * Define block regions; there's only one, so...
 */
function keepitsimple_regions() {
  return array(
    'left' => t('left sidebar'),
    'right' => t('right sidebar'),
    'content_bottom_left' => t('content bottom left'),
    'content_bottom_right' => t('content bottom right'),
    'footer_left' => t('footer left'),
    'footer_right' => t('footer right'),
  );
}

/**
 * Implementation of theme_menu_item().
 * Add active class and custom id to current menu item links.
 */
function phptemplate_menu_item($mid, $children = '', $leaf = TRUE) {
  $item = menu_get_item($mid); // get current menu item
  // decide whether to add the active class to this menu item

// print('PROVA');  
//  print('['.menu_get_active_item().']');
  
  if (menu_get_active_item() == $mid || (drupal_get_normal_path($item['path']) == $_GET['q']) // if menu item path...
  || (drupal_is_front_page() && $item['path'] == '<front>')) { // or front page...
    $active_class = ' current'; // set active class
  } else { // otherwise...
    $active_class = ''; // do nothing
  }
  $attribs = isset($item['description']) ? array('title' => $item['description']) : array();
  $replace = array(' ', '&');
  $attribs['id'] = 'menu-'. str_replace($replace, '-', strtolower($item['title']));
  return '<li class="'. ($leaf ? 'leaf' : ($children ? 'expanded' : 'collapsed')) .
    $active_class .'" id="'. $attribs['id'] . '">' .
    menu_item_link($mid) . $children ."</li>\n";
}


/**
 * Add "current" class to the li containing an a.active item.
 */
function phptemplate_links($links, $attributes = array('class' => 'links')) {
  $res = theme_links($links, $attributes);
  $res = preg_replace('/(<li[^>]* class="[^"]*)("><a[^>]*class=".* active">)/','$1 current$2',$res);
  return $res;
}
 
/**
 * type. all, category, tags.
 */
function _keepitsimple_theme_terms(&$links, $attributes = array('class' => 'links')) {
  $output = '';

  if (count($links) > 0) {
    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = $key;
      
      if ($i > 1) $output .= ', ';

      // Automatically add a class to each link and also to each LI
      if (isset($link['attributes']) && isset($link['attributes']['class'])) {
        $link['attributes']['class'] .= ' ' . $key;
      }
      else {
        $link['attributes']['class'] = $key;
      }

      // Is the title HTML?
      $html = isset($link['html']) && $link['html'];

      // Initialize fragment and query variables.
      $link['query'] = isset($link['query']) ? $link['query'] : NULL;
      $link['fragment'] = isset($link['fragment']) ? $link['fragment'] : NULL;

      if (isset($link['href'])) {
        $output .= l($link['title'], $link['href'], $link['attributes'], $link['query'], $link['fragment'], FALSE, $html);
      }
      else if ($link['title']) {
        //Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (!$html) {
          $link['title'] = check_plain($link['title']);
        }
        $output .= '<span'. drupal_attributes($link['attributes']) .'>'. $link['title'] .'</span>';
      }

      $i++;
    }
  }
  return $output;
}

/**
 * Override phptemplate_variables().
 */
function _phptemplate_variables($hook, $vars) {
  switch ($hook) {
    case 'node':
      $node = $vars['node'];
      
      if (module_exists('taxonomy')) {
        foreach (taxonomy_get_vocabularies($vars['node']->type) as $vid=>$vocab) {
          foreach (taxonomy_node_get_terms_by_vocabulary($vars['node']->nid, $vid) as $term) {
            if ($vocab->tags) {
              $tag_links['tag_link-'. $term->tid] = array(
                'title' => $term->name,
                'href' => taxonomy_term_path($term),
                'attributes' => array('rel' => 'tag','title' => strip_tags($term->description)),
              );
            }
            else {
              $term_links['term_link-'. $term->tid] = array(
                'title' => $term->name,
                'href' => taxonomy_term_path($term),
                'attributes' => array('rel' => 'tag','title' => strip_tags($term->description)),
              );
            }
          }
        }
      }
      else {
        $term_links = array();
        $tag_links = array();
      }
  
      $vars['terms'] = _keepitsimple_theme_terms($term_links);
      $vars['tags']  = _keepitsimple_theme_terms($tag_links);

      // Variables for the calendar date display.
      $vars['month'] = date('M', $node->created);
      $vars['day'] = date('j', $node->created);

      if (isset($node->links)) {
          $links = $node->links;
          $vars['links'] = theme('links', $links, array('class' => 'links inline'));
      }
      break;
    case 'page':
      $res = theme_eh_variables($hook, $vars);
  
      if ($hook == 'page') {
      if(module_exists('javascript_aggregator') && $vars['scripts'])
        $res['scripts'] = javascript_aggregator_cache($vars['scripts']);
      }

      return $res;
      break;
  }

  return $vars;
}

/**
 * Override theme_comment_wrapper() to turn comments into ordered list. 
 */
function keepitsimple_comment_wrapper($content, $type = null) {
  $output  = '<div id="commentblock">';
  $output .= '<ol class="commentlist">';
  $output .= $content;
  $output .= '</ol>';
  $output .= '</div>';

  return $output;
}

/**
 * Override for theme_textarea(); reduce size of comments field.
 */
function keepitsimple_textarea($element) {
  if ($element['#id'] == 'edit-comment') {
    $element['#cols'] = 50;
    $element['#rows'] = 5;
  }
  return theme_textarea($element);
}


/**
 * Return a themed link with a favicon.
 */
function keepitsimple_urlicon($text, $favicon, $path, $attributes = array()) {
  $favicon = '<img src="'. $favicon .'" '. drupal_attributes($attributes) .' />';
  return l($favicon . ' ' . $text, $path, array(), NULL, NULL, TRUE, TRUE);
} 
/*******************************************************************************
 * VIEWS
 ******************************************************************************/

/**
 * views template to output a view.
 * This code was generated by the views theming wizard
 * Date: 9 Febbraio 2009 - 17:35
 * View: feeds
 *
 * This function goes in your template.php file
 */
function phptemplate_views_view_list_feeds($view, $nodes, $type) {
  $fields = _views_get_fields();

  $taken = array();

  // Set up the fields in nicely named chunks.
  foreach ($view->field as $id => $field) {
    $field_name = $field['field'];
    if (isset($taken[$field_name])) {
      $field_name = $field['queryname'];
    }
    $taken[$field_name] = true;
    $field_names[$id] = $field_name;
  }

  // Set up some variables that won't change.
  $base_vars = array(
    'view' => $view,
    'view_type' => $type,
  );

  foreach ($nodes as $i => $node) {
    $vars = $base_vars;
    $vars['node'] = $node;
    $vars['count'] = $i;
    $vars['stripe'] = $i % 2 ? 'even' : 'odd';
    $vars['month'] = date('M', $node->node_created_created);
    $vars['day'] = date('j', $node->node_created_created);
    foreach ($view->field as $id => $field) {
      $name = $field_names[$id];
      $vars[$name] = views_theme_field('views_handle_field', $field['queryname'], $fields, $field, $node, $view);
      if (isset($field['label'])) {
        $vars[$name . '_label'] = $field['label'];
      }
    }
    $items[] = _phptemplate_callback('views-list-feeds', $vars);
  }
  if ($items) {
    return implode('', $items);
  }
}

/**
 * views template to output a view.
 * This code was generated by the views theming wizard
 * Date: 9 Febbraio 2009 - 18:28
 * View: feedlist
 *
 * This function goes in your template.php file
 */
function phptemplate_views_view_list_feedlist($view, $nodes, $type) {
  $fields = _views_get_fields();

  $taken = array();
  // Group our nodes
  $set = array();

  // Set up the fields in nicely named chunks.
  foreach ($view->field as $id => $field) {
    $field_name = $field['field'];
    if (isset($taken[$field_name])) {
      $field_name = $field['queryname'];
    }
    $taken[$field_name] = true;
    $field_names[$id] = $field_name;
  }

  // Set up some variables that won't change.
  $base_vars = array(
    'view' => $view,
    'view_type' => $type,
  );

  $nodevars = array();
  foreach ($nodes as $i => $node) {
    $vars = array();
    $vars += $base_vars;
    $vars['node'] = $node;
    $vars['count'] = $i;
    $vars['stripe'] = $i % 2 ? 'even' : 'odd';
    foreach ($view->field as $id => $field) {
      $name = $field_names[$id];
      $vars[$name] = views_theme_field('views_handle_field', $field['queryname'], $fields, $field, $node, $view);
      if (isset($field['label'])) {
        $vars[$name . '_label'] = $field['label'];
      }
    }
    $nodevars[$i] = $vars;
  }

  foreach ($nodevars as $vars) {
    $set[$vars['name']][] = $vars;
  }
  
  $output = '';
  foreach ($set as $label => $varslist) {
    $items = array();
    foreach ($varslist as $i => $vars) {
      $items[] = _phptemplate_callback('views-list-feedlist', $vars);
    }
    if ($items) {
      $output .= theme('item_list', $items, $label);
    }
  }
  return $output;
}