Drupal 5 port of StyleShout's theme named "KeepItSimple" created/sponsored
by Email Marketing Blog - http://emailmarketingblog.it

It's a 960px grid-based, minimalist website template. Ideal for news websites
and blogs with lots of contents.

I added "calendars" to blog entries and some specific handling to separate
taxonomy tags from taxonomy categories.

============================================================================
IMPORTANT - INSTALLATION INSTRUCTIONS
============================================================================

In order to use this theme you'll have to download the KeepItSimple style
distributed under the Creative Commons Attribution 2.5 License and unpack
its content in the keepitsimple theme folder.

http://www.styleshout.com/templates/downloads/KeepItSimple1-0.zip





Here are the full terms for that file
-------------------------------------------------
    All the free CSS templates on this site are distributed under the Creative 
    Commons Attribution 2.5 License. This means that you are free:

        * to copy, distribute, display, and perform the work
        * to make derivative works
        * to make commercial use of the work

    Under the following conditions:

        * You must attribute the work in the manner specified by the author or
          licensor. (In this case, a link back to my site)
        * For any reuse or distribution, you must make clear to others the
          license terms of this work
        * Any of these conditions can be waived if you get permission from the
          copyright holder
-------------------------------------------------